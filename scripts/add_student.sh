#!/usr/bin/env bash
set -euo pipefail

filename=eleves.csv
shell_input=""

while read line
do
    IFS=';' read -ra fields <<< "$line"

    uid=${fields[1]}
    name=${fields[2]}
    firstname=${fields[3]}
    mail=${fields[4]}

    shell_input="${shell_input}student ${uid};${mail};${name};${firstname}\n"

done < $filename

echo -e $shell_input | blockshell init
