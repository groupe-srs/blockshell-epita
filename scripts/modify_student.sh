#!/usr/bin/env bash
set -euo pipefail

filename=eleves.csv

student_23_uid=""
student_23_mail=""
student_23_name=""
student_23_firstname=""

student_24_uid=""
student_24_mail=""
student_24_name=""
student_24_firstname=""

count=1

while read line
do
    IFS=';' read -ra fields <<< "$line"

    if [ $count -eq 23 ]
    then
        IFS=';' read -ra fields <<< "$line"

        student_23_uid=${fields[1]}
        student_23_name=${fields[2]}
        student_23_firstname=${fields[3]}
        student_23_mail=${fields[4]}
    elif [ $count -eq 24 ]
    then
        IFS=';' read -ra fields <<< "$line"

        student_24_uid=${fields[1]}
        student_24_name=${fields[2]}
        student_24_firstname=${fields[3]}
        student_24_mail=${fields[4]}
    fi

    count=$((count+1))

done < $filename

shell_input="student_modify ${student_23_uid};${student_23_mail};${student_24_name};${student_23_firstname}\n"
shell_input="${shell_input}student_modify ${student_24_uid};${student_24_mail};${student_23_name};${student_24_firstname}\n"

echo -e $shell_input | blockshell init
