# -*- coding: utf-8 -*-
# ===================================================
# ==================== META DATA ===================
# ==================================================
__author__ = "Daxeel Soni"
__url__ = "https://daxeel.github.io"
__email__ = "daxeelsoni44@gmail.com"
__license__ = "MIT"
__version__ = "0.1"
__maintainer__ = "Daxeel Soni"

# ==================================================
# ================= IMPORT MODULES =================
# ==================================================
import click
import urllib
import random
import json
import base64
import numpy as np
from blockchain.chain import Block, Blockchain
from PIL import Image
from io import BytesIO


# ==================================================
# ===== SUPPORTED COMMANDS LIST IN BLOCKSHELL ======
# ==================================================
SUPPORTED_COMMANDS = [
    'dotx',
    'student',
    'student_modify',
    'allblocks',
    'getblock',
    'help'
]

# Init blockchain
coin = Blockchain()

# Create group of commands
@click.group()
def cli():
    """
        Create a group of commands for CLI
    """
    pass

# ==================================================
# ============= BLOCKSHELL CLI COMMAND =============
# ==================================================
@cli.command()
@click.option("--difficulty", default=3, help="Define difficulty level of blockchain.")
def init(difficulty):
    """Initialize local blockchain"""
    print ("""
  ____    _                  _       _____   _              _   _
 |  _ \  | |                | |     / ____| | |            | | | |
 | |_) | | |   ___     ___  | | __ | (___   | |__     ___  | | | |
 |  _ <  | |  / _ \   / __| | |/ /  \___ \  | '_ \   / _ \ | | | |
 | |_) | | | | (_) | | (__  |   <   ____) | | | | | |  __/ | | | |
 |____/  |_|  \___/   \___| |_|\_\ |_____/  |_| |_|  \___| |_| |_|

 > A command line utility for learning Blockchain concepts.
 > Type 'help' to see supported commands.
 > Project by Daxeel Soni - https://daxeel.github.io

    """)

    # Set difficulty of blockchain
    coin.difficulty = difficulty

    # Start blockshell shell
    while True:
        cmd = input("[BlockShell] $ ")
        processInput(cmd)


# Process input from Blockshell shell
def processInput(cmd):
    """
        Method to process user input from Blockshell CLI.
    """
    userCmd = cmd.split(" ")[0]
    if len(cmd) > 0:
        if userCmd in SUPPORTED_COMMANDS:
            globals()[userCmd](cmd)
        else:
            # error
            msg = "Command not found. Try help command for documentation"
            throwError(msg)


# ==================================================
# =========== BLOCKSHELL COMMAND METHODS ===========
# ==================================================
def random_base64_image():
    """
        Return a random 64x64 base64 image
    """
    array = np.random.rand(64, 64, 3) * 255
    image = Image.fromarray(array, 'RGB')
    buffered = BytesIO()
    image.save(buffered, format="PNG")
    return base64.b64encode(buffered.getvalue())


def student(cmd):
    """
        Create student block with uid, email, name and firstname
    """
    parsed = cmd.split("student")[-1]
    parsed = parsed.split(';')
    image = random_base64_image()
    dictionary = {
        'uid-epita': parsed[0],
        'email-epita': parsed[1],
        'nom': parsed[2],
        'prénom': parsed[3],
        'image': image.decode('utf-8')
    }
    data_json = json.dumps(dictionary, indent=4)
    coin.addBlock(Block(data=data_json))


def student_modify(cmd):
    """
        Modify student block giving its uid and new values
    """
    global coin

    parsed = cmd.split("student_modify")[-1]
    parsed = parsed.split(';')
    uid = parsed[0]
    email = parsed[1]
    name = parsed[2]
    firstname = parsed[3]

    count_element = 0

    modify = False
    new_coin = Blockchain()

    for block in coin.chain:
        plain = False
        try:
            data = json.loads(block.data)
        except:
            # Data is not json formatted
            data = block.data
            plain = True

        if modify:
            modified_data = data
            data_json = json.dumps(modified_data, indent=4)
            print("Modification indirecte de " + data['nom'])
            new_coin.addBlock(Block(data=data_json))

        elif not plain and data['uid-epita'] == uid:
            modify = True
            prev = data['nom']

            new_coin.chain = coin.chain[:count_element]

            modified_data = data
            modified_data['email-epita'] = email
            modified_data['nom'] = name
            modified_data['prénom'] = firstname
            data_json = json.dumps(modified_data, indent=4)
            print("Modification directe de " + prev + " en " + name)
            new_coin.addBlock(Block(data=data_json))

        count_element += 1

    coin = new_coin


def dotx(cmd):
    """
        Do Transaction - Method to perform new transaction on blockchain.
    """
    txData = cmd.split("dotx ")[-1]
    print(txData)
    if "{" in txData:
        txData = json.loads(txData)
    print("Doing transaction...")
    coin.addBlock(Block(data=txData))


def allblocks(cmd):
    """
        Method to list all mined blocks.
    """
    print("")
    for eachBlock in coin.chain:
        print(eachBlock.hash)
    print("")


def getblock(cmd):
    """
        Method to fetch the details of block for given hash.
    """
    blockHash = cmd.split(" ")[-1]
    for eachBlock in coin.chain:
        if eachBlock.hash == blockHash:
            print("")
            print(eachBlock.__dict__)
            print("")


def help(cmd):
    """
        Method to display supported commands in Blockshell
    """
    print("Commands:")
    print("   dotx <transaction data>    Create new transaction")
    print("   allblocks                  Fetch all mined blocks in blockchain")
    print("   getblock <block hash>      Fetch information about particular block")


def throwError(msg):
    """
        Method to throw an error from Blockshell.
    """
    print("Error : " + msg)
